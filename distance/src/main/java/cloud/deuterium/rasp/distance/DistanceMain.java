package cloud.deuterium.rasp.distance;

import com.pi4j.io.gpio.*;

/**
 * Created by MilanNuke 16-Nov-19
 *  https://thepihut.com/blogs/raspberry-pi-tutorials/hc-sr04-ultrasonic-range-sensor-on-the-raspberry-pi
 *
 */
public class DistanceMain {

    //GPIO Pins
    private static GpioPinDigitalOutput sensorTriggerPin;
    private static GpioPinDigitalInput sensorEchoPin;


    final static GpioController gpio = GpioFactory.getInstance();

    public static void main(String[] args) throws InterruptedException {
        new DistanceMain().run();
    }

    public void run() throws InterruptedException {
        sensorTriggerPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04); // Trigger pin as OUTPUT
        sensorEchoPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_05, PinPullResistance.PULL_DOWN); // Echo pin as INPUT

        while (true) {
            try {
                Thread.sleep(2000);
                sensorTriggerPin.high(); // Make trigger pin HIGH
                Thread.sleep((long) 0.01);// Delay for 10 microseconds
                sensorTriggerPin.low(); //Make trigger pin LOW

                while (sensorEchoPin.isLow()) { //Wait until the ECHO pin gets HIGH

                }
                long startTime = System.nanoTime(); // Store the surrent time to calculate ECHO pin HIGH time.
                while (sensorEchoPin.isHigh()) { //Wait until the ECHO pin gets LOW

                }
                long endTime = System.nanoTime(); // Store the echo pin HIGH end time to calculate ECHO pin HIGH time.

                System.out.println("Distance :" + ((((endTime - startTime) / 1e3) / 2) / 29.1) + " cm"); //Printing out the distance in cm
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
