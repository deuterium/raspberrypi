package cloud.deuterium.rasp.light;

import com.pi4j.io.gpio.*;
import com.pi4j.wiringpi.Gpio;

/**
 * Created by MilanNuke 10-Nov-19
 */
public class LightMain {

    public static void main(String[] args) {

        final GpioController gpio = GpioFactory.getInstance();
        final GpioPinDigitalOutput output = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, "MyLED", PinState.LOW);

        System.out.println("Start");
        for (int i = 0; i < 10; i++) {
            output.high();
            System.out.printf("Blink: %d%n", i);
            Gpio.delay(500);
            output.low();
            Gpio.delay(500);
        }
        System.out.println("Exit");

    }

}
