package cloud.deuterium.rasp.rain;

import com.pi4j.io.gpio.*;
import com.pi4j.wiringpi.Gpio;

/**
 * Created by MilanNuke 17-Nov-19
 *  https://raspi.tv/2017/make-a-rain-alert-system-with-raspberry-pi
 *
 */
public class RainMain {

    public static void main(String[] args) {

        final GpioController gpio = GpioFactory.getInstance();
        final GpioPinDigitalInput input = gpio.provisionDigitalInputPin(RaspiPin.GPIO_06, PinPullResistance.PULL_DOWN);// DO pin as INPUT
        final GpioPinDigitalOutput output = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, "MyLED", PinState.LOW); // Za led diodu

        for(int i = 0; i < 100; i++){
            System.out.print("Ciklus: " + i);
            if(input.isLow()){
                System.out.print(" Ima kise!!!!");
                output.high();
            }else {
                output.low();
            }
            System.out.println();
            Gpio.delay(500);
        }

        output.low();
        System.out.println("End...................");
    }
}
