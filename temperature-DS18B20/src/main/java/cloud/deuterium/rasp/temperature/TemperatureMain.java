package cloud.deuterium.rasp.temperature;

import cloud.deuterium.rasp.temperature.model.TempUtil;
import cloud.deuterium.rasp.temperature.sensor.TSensor;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by MilanNuke 08-Nov-19
 */
public class TemperatureMain {
    public static void main(String[] args) {
        System.out.println("Start...");
        TSensor tSensor = new TSensor();
        HttpClient client = HttpClient.newHttpClient();
        String url = "http://192.168.0.103:8080/temperature";
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        Runnable task = () -> {
            double temperature = tSensor.getTemperature();
            System.out.printf("Temperature: %f %n", temperature);
            try {
                HttpRequest request = HttpRequest
                        .newBuilder(new URI(url))
                        .header("Content-Type", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(TempUtil.getString(temperature)))
                        .build();
                HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
                if(response.statusCode() == 200){
                    System.out.println("Temperature successfully sent to server");
                }else{
                    System.out.printf("Server is unavailable! Status: %d%n", response.statusCode());
                }
            } catch (URISyntaxException | InterruptedException | IOException e) {
                e.printStackTrace();
            }
        };

        executorService.scheduleAtFixedRate(task, 2, 3, TimeUnit.SECONDS);
    }
}
