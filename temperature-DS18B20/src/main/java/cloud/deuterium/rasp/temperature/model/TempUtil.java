package cloud.deuterium.rasp.temperature.model;

/**
 * Created by MilanNuke 15-Oct-19
 */
public class TempUtil {

    private static final String host = "192.168.0.103";
    private static final String sensor = "DS18B20";

    public static String getString(double temperature){
        return "{\"host\":\"" + host + "\"," +
                "\"sensor\":\"" + sensor + "\"," +
                "\"value\":\"" + temperature + "\"}";
    }

}
