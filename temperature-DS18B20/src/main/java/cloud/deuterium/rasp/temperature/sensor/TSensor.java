package cloud.deuterium.rasp.temperature.sensor;

import com.pi4j.component.temperature.TemperatureSensor;
import com.pi4j.io.w1.W1Master;
import com.pi4j.temperature.TemperatureScale;

/**
 * Created by MilanNuke 08-Nov-19
 */
public class TSensor {

    public double getTemperature(){
        W1Master w1Master = new W1Master();
        for(TemperatureSensor device : w1Master.getDevices(TemperatureSensor.class)){
            if(device.getName().contains("28-00000afebf9d")){
                return device.getTemperature(TemperatureScale.CELSIUS);
            }
        }
        return 0;
    }
}
