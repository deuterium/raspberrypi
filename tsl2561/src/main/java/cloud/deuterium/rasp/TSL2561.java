package cloud.deuterium.rasp;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

/**
 * Created by MilanNuke 15-Dec-19
 */

public class TSL2561 {
    public static void main(String[] args) throws Exception {
        // Create I2C bus
        I2CBus Bus = I2CFactory.getInstance(I2CBus.BUS_1);
        // Get I2C device, TSL2561 I2C address is 0x39(57)
        I2CDevice device = Bus.getDevice(0x39);

        for (int i = 0; i < 60; i++) {
            getMeasurements(device);
            Thread.sleep(1000);
        }
    }

    private static void getMeasurements(I2CDevice device) throws Exception {
        // Select control register
        // Power ON mode
        device.write(0x00 | 0x80, (byte) 0x03);
        // Select timing register
        // Nominal integration time = 402ms
        device.write(0x01 | 0x80, (byte) 0x02);
        Thread.sleep(500);

        // Read 4 bytes of data
        // ch0 lsb, ch0 msb, ch1 lsb, ch1 msb
        byte[] data = new byte[4];
        device.read(0x0C | 0x80, data, 0, 4);

        // Convert the data
        double ch0 = ((data[1] & 0xFF) * 256 + (data[0] & 0xFF));
        double ch1 = ((data[3] & 0xFF) * 256 + (data[2] & 0xFF));

        // Output data to screen
        System.out.printf("Full Spectrum(IR + Visible) : %.2f lux %n", ch0);
        System.out.printf("Infrared Value : %.2f lux %n", ch1);
        System.out.printf("Visible Value : %.2f lux %n", (ch0 - ch1));
    }
}
